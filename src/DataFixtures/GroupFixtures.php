<?php

namespace App\DataFixtures;

use App\Entity\Group;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class GroupFixtures extends Fixture
{

    public function load(ObjectManager $manager): void
    {
    
        for ($i = 0; $i < 20; $i++) {
            $group = new Group();
            $group->setLabel('group '.$i);
            $manager->persist($group);
        }
        
        $manager->flush();
        
    }
}
