<?php

namespace App\DataFixtures;

use App\Entity\Ressource;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class RessourceFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < 20; $i++) {
            $ressource = new Ressource();
            $ressource->setLabel('produit '.$i);
            $ressource->setImage('https://images.unsplash.com/photo-1639433506588-f88a286d7e6d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=987&q=80');
            $ressource->setDescription('Je suis le produit '.$i);
            $ressource->setQuantity(rand($i,50));


            $manager->persist($ressource);
        }
        $manager->flush();
    }
}
