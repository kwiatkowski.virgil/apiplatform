<?php

namespace App\DataFixtures;
use App\Entity\User;
use App\Entity\Group;
use App\Controller\GroupController;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;


class UserFixtures extends Fixture  
{

    // public function __construct(UserPasswordHasherInterface $passwordHasher)
    // {
    //     $this->hasher=$passwordHasher;
    // }
    // public function load(ObjectManager $manager): void
    // {
    //     $groups = $manager
    //     ->getRepository(Group::class)
    //     ->findAll();

    // if (!$groups) {
    //     throw $this->createNotFoundException(
    //         'Pas de groupe'
    //     );
        public function load(ObjectManager $manager): void
    {
        $groups = $manager
        ->getRepository(Group::class)
        ->findAll();
        if (!$groups) {
            throw $this->createNotFoundException(
            'Pas de groupe'
        );
    }
        for ($i = 0; $i < 20; $i++) {
            $user = new User();
            $user->setFirstname('prenom '.$i);
            $user->setLastname('firstname '.$i);
            $user->setEmail('email '.$i);
            $user->setPassword('password '.$i);
            $user->setUserRole('admin');
            $manager->persist($user);
        }
        $manager->flush();
    }     
}